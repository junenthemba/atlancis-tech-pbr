-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `payment-gateway`;
CREATE DATABASE `payment-gateway` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `payment-gateway`;

DROP TABLE IF EXISTS `b2b_mpesa_request`;
CREATE TABLE `b2b_mpesa_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `shortCode` varchar(200) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `accountRef` varchar(200) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `resultUrl` varchar(200) DEFAULT NULL,
  `OriginatorConverstionID` varchar(200) DEFAULT NULL,
  `ConversationID` varchar(200) DEFAULT NULL,
  `ResponseDescription` varchar(200) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `b2b_transaction`;
CREATE TABLE `b2b_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `resultType` int(11) NOT NULL,
  `resultCode` int(11) NOT NULL,
  `resultDesc` varchar(200) NOT NULL,
  `originatorConversationID` varchar(200) NOT NULL,
  `conversationID` varchar(200) NOT NULL,
  `transactionID` varchar(200) NOT NULL,
  `debitAccountBalance` varchar(200) DEFAULT NULL,
  `initiatorAccountCurrentBalance` varchar(200) DEFAULT NULL,
  `debitAccountCurrentBalance` varchar(200) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `transCompletedTime` datetime DEFAULT NULL,
  `debitPartyCharges` varchar(200) DEFAULT NULL,
  `creditPartyPublicName` varchar(200) DEFAULT NULL,
  `debitPartyPublicName` varchar(200) DEFAULT NULL,
  `creditAccountBalance` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transactionIDUnique` (`transactionID`),
  UNIQUE KEY `originatorConversationIDUnique` (`originatorConversationID`),
  UNIQUE KEY `conversationIDUnique` (`conversationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `c2b_transaction`;
CREATE TABLE `c2b_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `TransactionType` varchar(100) NOT NULL,
  `TransID` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `MiddleName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `MSISDN` varchar(100) NOT NULL,
  `TransAmount` double NOT NULL,
  `TransTime` datetime NOT NULL,
  `OrgAccountBalance` double NOT NULL,
  `BusinessShortCode` varchar(30) NOT NULL,
  `BillRefNumber` varchar(100) NOT NULL,
  `InvoiceNumber` varchar(100) DEFAULT NULL,
  `ThirdPartyTransID` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transID` (`TransID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lnm_mpesa_request`;
CREATE TABLE `lnm_mpesa_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `phoneNumber` varchar(200) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `accountRef` varchar(200) DEFAULT NULL,
  `transactionDesc` varchar(200) DEFAULT NULL,
  `resultUrl` varchar(200) DEFAULT NULL,
  `requestStatus` varchar(200) NOT NULL,
  `merchantRequestID` varchar(200) DEFAULT NULL,
  `checkoutRequestID` varchar(200) DEFAULT NULL,
  `responseDescription` varchar(200) NOT NULL,
  `responseCode` int(11) DEFAULT NULL,
  `customerMessage` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lnm_mpesa_transaction`;
CREATE TABLE `lnm_mpesa_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `merchantRequestID` varchar(100) NOT NULL,
  `checkoutRequestID` varchar(100) NOT NULL,
  `resultCode` int(11) NOT NULL,
  `resultDesc` varchar(200) NOT NULL,
  `phoneNumber` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `mpesaReceiptNumber` varchar(100) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `transactionDate` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mpesaReceiptNumber` (`mpesaReceiptNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mpesa_Request`;
CREATE TABLE `mpesa_Request` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `requestDetails` longtext NOT NULL,
  `requestType` varchar(200) NOT NULL,
  `requestStatus` varchar(200) NOT NULL,
  `requestResult` longtext,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2018-03-29 07:30:30
