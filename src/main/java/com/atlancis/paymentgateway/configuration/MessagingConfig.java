/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author nthemba
 * resources
 * multiple queues: https://stackoverflow.com/questions/41210688/mutliple-rabbitmq-queues-with-spring-boot
 */
@SpringBootApplication
@EnableRabbit
@EnableScheduling
public class MessagingConfig  {
    
    public static final String QUEUE_LNM = "lnm.send";
    public static final String ROUTING_KEY_LNM = "lnm.send";
    public static final String QUEUE_RESPONSE = "mpesaResponse.send";
    public static final String ROUTING_KEY_RESPONSE = "mpesaResponse.send";
    public static final String EXCHANGE_NAME = "";
    
    public DirectExchange appExchange() {
        return new DirectExchange(EXCHANGE_NAME);
    }

    @Bean
    public Queue mpesaSendMoney() {
        return new Queue(QUEUE_LNM);
    }

    @Bean
    public Binding declareBindingLNMQueue() {
        return BindingBuilder.bind(mpesaSendMoney()).to(appExchange()).with(ROUTING_KEY_LNM);
    }
    
    @Bean
    public Queue sendMpesaResponse(){
        return new Queue(QUEUE_RESPONSE);
    }
    
    @Bean
    public Binding declareBindingResponse(){
        return BindingBuilder.bind(sendMpesaResponse()).to(appExchange()).with(ROUTING_KEY_RESPONSE);
    }
    

}
