/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author nthemba
 */

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("mpesaapi")
public class MpesaConfig {
    
    private String ngrok;
    private String lnmShortcode;
    private String lnmCallbackUrl;
    private String passkey;
    private String lnmSandBoxUrl;
    private String appkey;
    private String appSecret;
    private String authSandBoxUrl;
    private String c2bConfirmUrl;
    private String c2bValidationUrl;
    private String c2bSimulateSandBoxUrl;
    private String b2bSandBoxUrl;
    private String b2bQueueTimeOutUrl;
    private String b2bResultUrl;
    private String shortcodePartyA;
    private String initiator;
    private String securityCredential;
    private String senderIdentifierMSISDN;
    private String senderIdentifierTillNumber;
    private String senderIdentifierOrganizationShortcode;
    private String b2cSandBoxUrl;
    private String b2cQueueTimeOutUrl;
    private String b2cResultUrl;
    private String accountBalanceSandBoxUrl;
    private String accBalQueueTimeOutUrl;
    private String accBalResultUrl;
    private String transactionStatusSandBoxUrl;
    private String transactionStatusQueueTimeOutUrl;
    private String transactionStatusResultUrl;
    private String c2bRegisterUrlSandBoxUrl;

    public String getC2bRegisterUrlSandBoxUrl() {
        return c2bRegisterUrlSandBoxUrl;
    }

    public void setC2bRegisterUrlSandBoxUrl(String c2bRegisterUrlSandBoxUrl) {
        this.c2bRegisterUrlSandBoxUrl = c2bRegisterUrlSandBoxUrl;
    }
    
    public String getTransactionStatusResultUrl() {
        return transactionStatusResultUrl;
    }

    public void setTransactionStatusResultUrl(String transactionStatusResultUrl) {
        this.transactionStatusResultUrl = transactionStatusResultUrl;
    }
    
    public String getTransactionStatusQueueTimeOutUrl() {
        return transactionStatusQueueTimeOutUrl;
    }

    public void setTransactionStatusQueueTimeOutUrl(String transactionStatusQueueTimeOutUrl) {
        this.transactionStatusQueueTimeOutUrl = transactionStatusQueueTimeOutUrl;
    }

    public String getTransactionStatusSandBoxUrl() {
        return transactionStatusSandBoxUrl;
    }

    public void setTransactionStatusSandBoxUrl(String transactionStatusSandBoxUrl) {
        this.transactionStatusSandBoxUrl = transactionStatusSandBoxUrl;
    }
    
    public String getAccBalResultUrl() {
        return accBalResultUrl;
    }

    public void setAccBalResultUrl(String accBalResultUrl) {
        this.accBalResultUrl = accBalResultUrl;
    }
    
    public String getAccBalQueueTimeOutUrl() {
        return accBalQueueTimeOutUrl;
    }

    public void setAccBalQueueTimeOutUrl(String accBalQueueTimeOutUrl) {
        this.accBalQueueTimeOutUrl = accBalQueueTimeOutUrl;
    }
    
    public String getAccountBalanceSandBoxUrl() {
        return accountBalanceSandBoxUrl;
    }

    public void setAccountBalanceSandBoxUrl(String accountBalanceSandBoxUrl) {
        this.accountBalanceSandBoxUrl = accountBalanceSandBoxUrl;
    }
   
    public String getB2cResultUrl() {
        return b2cResultUrl;
    }

    public void setB2cResultUrl(String b2cResultUrl) {
        this.b2cResultUrl = b2cResultUrl;
    }

    public String getB2cQueueTimeOutUrl() {
        return b2cQueueTimeOutUrl;
    }

    public void setB2cQueueTimeOutUrl(String b2cQueueTimeOutUrl) {
        this.b2cQueueTimeOutUrl = b2cQueueTimeOutUrl;
    }
    
    public String getB2cSandBoxUrl() {
        return b2cSandBoxUrl;
    }

    public void setB2cSandBoxUrl(String b2cSandBoxUrl) {
        this.b2cSandBoxUrl = b2cSandBoxUrl;
    }

    public String getSenderIdentifierMSISDN() {
        return senderIdentifierMSISDN;
    }

    public void setSenderIdentifierMSISDN(String senderIdentifierMSISDN) {
        this.senderIdentifierMSISDN = senderIdentifierMSISDN;
    }

    public String getSenderIdentifierTillNumber() {
        return senderIdentifierTillNumber;
    }

    public void setSenderIdentifierTillNumber(String senderIdentifierTillNumber) {
        this.senderIdentifierTillNumber = senderIdentifierTillNumber;
    }

    public String getSenderIdentifierOrganizationShortcode() {
        return senderIdentifierOrganizationShortcode;
    }

    public void setSenderIdentifierOrganizationShortcode(String senderIdentifierOrganizationShortcode) {
        this.senderIdentifierOrganizationShortcode = senderIdentifierOrganizationShortcode;
    }

    public String getSecurityCredential() {
        return securityCredential;
    }

    public void setSecurityCredential(String securityCredential) {
        this.securityCredential = securityCredential;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }
    
    public String getShortcodePartyA() {
        return shortcodePartyA;
    }

    public void setShortcodePartyA(String shortcodePartyA) {
        this.shortcodePartyA = shortcodePartyA;
    }

    public String getNgrok() {
        return ngrok;
    }

    public void setNgrok(String ngrok) {
        this.ngrok = ngrok;
    }

    public String getLnmShortcode() {
        return lnmShortcode;
    }

    public void setLnmShortcode(String lnmShortcode) {
        this.lnmShortcode = lnmShortcode;
    }

    public String getLnmCallbackUrl() {
        return lnmCallbackUrl;
    }

    public void setLnmCallbackUrl(String lnmCallbackUrl) {
        this.lnmCallbackUrl = lnmCallbackUrl;
    }

    public String getPasskey() {
        return passkey;
    }

    public void setPasskey(String passkey) {
        this.passkey = passkey;
    }

    public String getLnmSandBoxUrl() {
        return lnmSandBoxUrl;
    }

    public void setLnmSandBoxUrl(String lnmSandBoxUrl) {
        this.lnmSandBoxUrl = lnmSandBoxUrl;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAuthSandBoxUrl() {
        return authSandBoxUrl;
    }

    public void setAuthSandBoxUrl(String authSandBoxUrl) {
        this.authSandBoxUrl = authSandBoxUrl;
    }

    public String getC2bConfirmUrl() {
        return c2bConfirmUrl;
    }

    public void setC2bConfirmUrl(String c2bConfirmUrl) {
        this.c2bConfirmUrl = c2bConfirmUrl;
    }

    public String getC2bValidationUrl() {
        return c2bValidationUrl;
    }

    public void setC2bValidationUrl(String c2bValidationUrl) {
        this.c2bValidationUrl = c2bValidationUrl;
    }

    public String getC2bSimulateSandBoxUrl() {
        return c2bSimulateSandBoxUrl;
    }

    public void setC2bSimulateSandBoxUrl(String c2bSimulateSandBoxUrl) {
        this.c2bSimulateSandBoxUrl = c2bSimulateSandBoxUrl;
    }

    public String getB2bSandBoxUrl() {
        return b2bSandBoxUrl;
    }

    public void setB2bSandBoxUrl(String b2bSandBoxUrl) {
        this.b2bSandBoxUrl = b2bSandBoxUrl;
    }

    public String getB2bQueueTimeOutUrl() {
        return b2bQueueTimeOutUrl;
    }

    public void setB2bQueueTimeOutUrl(String b2bQueueTimeOutUrl) {
        this.b2bQueueTimeOutUrl = b2bQueueTimeOutUrl;
    }

    public String getB2bResultUrl() {
        return b2bResultUrl;
    }

    public void setB2bResultUrl(String b2bResultUrl) {
        this.b2bResultUrl = b2bResultUrl;
    }
    
    
    
    
    

    
    
    
    
    
    
    
    
}
