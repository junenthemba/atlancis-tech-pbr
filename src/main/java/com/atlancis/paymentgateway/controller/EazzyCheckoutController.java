/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.controller;

import com.atlancis.paymentgateway.configuration.EazzyConfig;
import com.atlancis.paymentgateway.service.eazzyService.EazzyApiService;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nthemba
 */
@RestController
@RequestMapping("/eazzy/v1")
public class EazzyCheckoutController {
    
    @Autowired
    EazzyApiService eazzyService;
    
    @Autowired
    EazzyConfig eazzyConfig;
    
    private Logger log = Logger.getLogger(EazzyCheckoutController.class.getName());
    
    @RequestMapping(value = "/getToken", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAuthToken(){
        
        String response = eazzyService.getAuthToken();
        
        JSONObject json = new JSONObject(response);
        
        json.put("token", json.optString("access_token"));
        return new ResponseEntity<>(json.toString(), HttpStatus.OK);
          
    }
    
    @RequestMapping(value = "/getDetails", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getMerchantDetails(){
        String merchant = eazzyConfig.getMerchantName();
        String merchantCode = eazzyConfig.getMerchantCode();
        String outletCode = eazzyConfig.getOutletCode();
        
        JSONObject response = new JSONObject();
        response.put("merchant", merchant);
        response.put("merchantCode", merchantCode);
        response.put("outletCode", outletCode);
        
        return new ResponseEntity<>(response.toString(), HttpStatus.OK);
    }

}
