/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.controller;

import com.atlancis.paymentgateway.configuration.MpesaConfig;
import com.atlancis.paymentgateway.customEntitites.C2bResponse;
import com.atlancis.paymentgateway.service.dataservice.DataService;
import com.atlancis.paymentgateway.messaging.MpesaSendMoneySender;
import com.atlancis.paymentgateway.service.mpesaService.MpesaApiService;
import com.atlancis.paymentgateway.service.mpesaService.MpesaTransactionType;
import com.atlancis.paymentgateway.utils.PaymentUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.atlancis.paymentgateway.service.mpesaService.MpesaRequestService;
import com.atlancis.paymentgateway.service.mpesaService.RequestStatus;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

/**
 *
 * @author nthemba
 */
@RestController
@RequestMapping("/mpesaapi/v1")

public class MpesaApiController {

    private Logger log = Logger.getLogger(MpesaApiController.class.getName());

    @Autowired
    DataService dataService;
    
    @Autowired
    MpesaApiService mpesaService;

    @Autowired
    MpesaRequestService mpesaRequest;

    @Autowired
    MpesaConfig yamlConfig;

    @Autowired
    MpesaSendMoneySender mpesaSender;

    @RequestMapping(value = "/registerUrl", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String registerUrl() {
        String sandBoxUrl = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl";

        String request = mpesaRequest.registerUrlRequest();

        return mpesaService.sendRequest(sandBoxUrl, request);

    }

    @RequestMapping(value = "/c2b/confirmation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> confirmC2BRequest(HttpServletRequest httpRequest, @RequestBody C2bResponse c2bResponse) {
        log.log(Level.INFO, "CONFIRMATION " + c2bResponse.toString());
        
         String ipAddress = httpRequest.getRemoteAddr();
        
        dataService.mapToC2bTransaction(c2bResponse, ipAddress );
        
        String responseBody = mpesaRequest.c2bConfirmation();
        
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    @RequestMapping(value = "/c2b/validate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateC2BRequest(@RequestBody String response) {
        log.log(Level.INFO, "VALIDATION " + response);
   
        // Endpoint to be exposed to make transaction 
        String responseBody = mpesaRequest.c2bValidate();
        
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    //will not be needed in production
    @RequestMapping(value = "/C2BSimulate/{phonumber}/{amount}/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String simulateC2B(@PathVariable("phonumber") String phoneNumber, @PathVariable("amount") Double amount) {

        String sandboxUrl = yamlConfig.getC2bSimulateSandBoxUrl();
        String formatedPhoneNumber = PaymentUtils.formatPhoneNumber(phoneNumber);
        
        String requestBody = mpesaRequest.c2bSimulate(phoneNumber, amount);
        
        log.log(Level.INFO, requestBody);
        
        String response = mpesaService.sendRequest(sandboxUrl, requestBody);

//        JSONObject res = new JSONObject();
//        res.put("response", response);

        return response;

    }
    
        
    @RequestMapping(value = "/lipanampesa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> lipanampesaOnline(HttpServletRequest httpRequest, @RequestBody String request){
        String ipAddress = httpRequest.getRemoteAddr();

        String sandboxUrl = yamlConfig.getLnmSandBoxUrl();

        JSONObject json = new JSONObject(request);

        String phoneNumber = json.optString("phoneNumber");
        Double amount = json.optDouble("amount");

        JSONObject res = new JSONObject();
        String response = null;

        if (PaymentUtils.isNullOrEmpty(phoneNumber)) {
            response = "Phone Number not entered";
            log.log(Level.SEVERE, response);
            res.put("response", response);

            //save the mpesa request
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.LNM_TRANS, response);
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);

        } else if (amount == null) {
            response = "Amount not entered";
            log.log(Level.SEVERE, response);
            res.put("response", response);

            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.LNM_TRANS, response);
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);

        } else if (PaymentUtils.isValidPhoneNumber(phoneNumber)) {
            String requestBody = mpesaRequest.lipaNaMpesaRequest(json);
            response = mpesaService.sendRequest(sandboxUrl, requestBody);
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.SUCCESS, MpesaTransactionType.LNM_TRANS, response);
            return new ResponseEntity<>(response.toString(), HttpStatus.ACCEPTED);
        } else {
            response = "Invalid Phone number";
            log.log(Level.SEVERE, response);
            res.put("response", response);
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.LNM_TRANS, response);
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/lipanampesa/callback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void lipanampesaCallback(HttpServletRequest httpRequest, @RequestBody String response) {
        log.log(Level.INFO, response);

        String ipAddress = httpRequest.getRemoteAddr();
        dataService.mapToLnmMpesaTransaction(response,ipAddress);
        
        //initiate producer to send message to respective application
    }
    
    @RequestMapping(value = "/b2b/request", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> B2BPaymentRequest(HttpServletRequest httpRequest, @RequestBody String request) {
        String ipAddress = httpRequest.getRemoteAddr();
        
        String sandboxUrl = yamlConfig.getB2bSandBoxUrl();
        
        JSONObject json = new JSONObject(request);

        String shortCode = json.optString("shortCode");
        Double amount = json.optDouble("amount");

        String response = null;

        if (PaymentUtils.isNullOrEmpty(shortCode)) {
            response = "ShortCode not entered";
            log.log(Level.SEVERE, response);
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.B2B_TRANS, response);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            
        } else if (amount == null) {
            response = "Amount not entered";
            log.log(Level.SEVERE, response);
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.B2B_TRANS, response);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            
        } else {
            String requestBody = mpesaRequest.b2bPaymentRequest(shortCode, amount);
            response = mpesaService.sendRequest(sandboxUrl, requestBody);
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.SUCCESS, MpesaTransactionType.B2B_TRANS, response);
            return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
        }
    }

    //  //requires queue
    @RequestMapping(value = "/b2b/queueTimeOut", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void B2BQueueTimeout(@RequestBody String response) {
        log.log(Level.INFO, ">>>>##### QUEUE TIMEOUT " + response);
        

    }

    @RequestMapping(value = "/b2b/result", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void B2BResultURL(HttpServletRequest httpRequest, @RequestBody String response) {
        String ipAddress = httpRequest.getRemoteAddr();
        log.log(Level.INFO, ">>>>##### RESULT URL " + response);

        dataService.mapToB2bTransaction(response,ipAddress);
        
        //return response to request initializer
    }

    @RequestMapping(value = "/b2c/request", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> B2CPaymentRequest(HttpServletRequest httpRequest, @RequestBody String request) {
        String ipAddress = httpRequest.getRemoteAddr();
        
        String sandboxUrl = yamlConfig.getB2cSandBoxUrl();
        
        JSONObject json = new JSONObject(request);
        
        String phoneNumber = json.optString("phoneNumber");
        Double amount = json.optDouble("amount");

        JSONObject res = new JSONObject();
        
        String response = null;
        
        if(PaymentUtils.isNullOrEmpty(phoneNumber)){           
            response = "Phone Number not entered";
            log.log(Level.SEVERE, response); 
            res.put("response",response);
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.B2C_TRANS, response);
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
            
        }else if(amount == null){
            response = "Amount not entered";
            log.log(Level.SEVERE, response);
            res.put("response",response);
            dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.B2C_TRANS, response);
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
            
        }else{
            if(PaymentUtils.isValidPhoneNumber(phoneNumber)){
                String formatedPhoneNumber = PaymentUtils.formatPhoneNumber(phoneNumber);
                String requestBody = mpesaRequest.b2cPaymentRequest(formatedPhoneNumber, amount);
                response = mpesaService.sendRequest(sandboxUrl, requestBody);
                dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.SUCCESS, MpesaTransactionType.B2C_TRANS, response);
                return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
            }else{
                response = "Invalid Phone number";
                log.log(Level.SEVERE, response);
                res.put("response",response);
                dataService.mpesaRequestHelper(ipAddress, request, RequestStatus.FAIL, MpesaTransactionType.B2C_TRANS, response);
                return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
            } 
        }

    }

      //requires queue
    @RequestMapping(value = "/b2c/queueTimeout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void b2cQueueTimeout(@RequestBody String response) {
        log.log(Level.INFO, ">>>>##### Queue Timeout " + response);
    }

      //requires queue
    @RequestMapping(value = "/b2c/result", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void b2cResultUrl(@RequestBody String response) {
        log.log(Level.INFO, ">>>>##### Result Timeout " + response);
    }

    @RequestMapping(value = "/account/balance", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAccountBalance() {

        String sandBoxUrl = yamlConfig.getAccountBalanceSandBoxUrl();

        String request = mpesaRequest.getaccountBalance();

        return mpesaService.sendRequest(sandBoxUrl, request);
    }

    @RequestMapping(value = "/account/balance/queueTimeOut", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void accoutBalanceQueueTimeoutUrl(@RequestBody String response) {
        log.log(Level.INFO, response);
    }

    @RequestMapping(value = "/account/balance/result", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void accountBalanceResultURL(@RequestBody String response) {
        log.log(Level.INFO, response);
    }

    @RequestMapping(value = "/transactionQuery/{phoneNumber}/{transactionId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getTransactionQuery(@PathVariable("phoneNumber") String phoneNumber, @PathVariable("transactionId") String transactionId) {

        String formatedPhoneNumber = PaymentUtils.formatPhoneNumber(phoneNumber);
        
        String sandBoxUrl = yamlConfig.getTransactionStatusSandBoxUrl();

        String request = mpesaRequest.transactionStatus(phoneNumber, transactionId);

        return mpesaService.sendRequest(sandBoxUrl, request);
    }

    @RequestMapping(value = "/transactionQuery/queueTimeOut", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void transactionQueryQueueTimeoutUrl(@RequestBody String response) {
        log.log(Level.INFO, response);
    }

    @RequestMapping(value = "/transactionQuery/result", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void transactionQueryResultUrl(@RequestBody String response) {
        log.log(Level.INFO, response);
    }

}
