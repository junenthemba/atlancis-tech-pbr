/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.controller;

import com.atlancis.paymentgateway.configuration.MpesaConfig;
import com.atlancis.paymentgateway.customEntitites.B2BRequest;
import com.atlancis.paymentgateway.customEntitites.C2bResponse;
import com.atlancis.paymentgateway.customEntitites.LNMRequest;
import com.atlancis.paymentgateway.messaging.MpesaResponseSender;
import com.atlancis.paymentgateway.model.B2bMpesaRequest;
import com.atlancis.paymentgateway.model.LnmMpesaRequest;
import com.atlancis.paymentgateway.service.dataservice.DataService;
import com.atlancis.paymentgateway.service.mpesaService.MpesaApiService;
import com.atlancis.paymentgateway.service.mpesaService.MpesaRequestService;
import com.atlancis.paymentgateway.service.mpesaService.RequestStatus;
import com.atlancis.paymentgateway.utils.PaymentUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.atlancis.paymentgateway.repositories.LnmMpesaRequestRepository;
import com.atlancis.paymentgateway.service.mpesaService.MpesaTransactionType;

/**
 *
 * @author nthemba
 */
@RestController
@RequestMapping("/mpesaapi/v2")
public class MpesaApiControllerV2 {
    
    @Autowired
    MpesaRequestService mpesaRequest;
    
    @Autowired
    MpesaApiService mpesaService;
    
    @Autowired
    MpesaConfig mpesaConfig;
    
    @Autowired
    DataService dataService;
    
    @Autowired
    MpesaResponseSender mpesaResponseSender;
    
    
    
    private Logger log = Logger.getLogger(MpesaApiControllerV2.class.getName());
    
    
    @RequestMapping(value = "/registerUrl", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String registerUrl() {
        /**
         * Tells Safaricom which urls are active for validation and confirmation of c2b requests
         * Done only once
         * Create request body
         * Send request to safcom
         */
        String sandBoxUrl = mpesaConfig.getC2bRegisterUrlSandBoxUrl();

        String requestBody = mpesaRequest.registerUrlRequest();

        return mpesaService.sendRequest(sandBoxUrl, requestBody);

    }
    
    @RequestMapping(value = "/c2b/confirmation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> confirmC2BRequest(HttpServletRequest httpRequest, @RequestBody C2bResponse c2bResponse) {
        /**
         * receives the transaction details of validated c2b transactions
         * Map response to c2b object
         * Save transaction to db
         * send response to respective consumer
         * send response to safaricom
         */
        String ipAddress = httpRequest.getRemoteAddr();
        
        log.log(Level.INFO, "CONFIRMATION " + c2bResponse.toString() + " " + ipAddress);
        
        dataService.mapToC2bTransaction(c2bResponse, ipAddress );

        //send to owner
        //how do i identify what goes to whos
       
        String responseBody = mpesaRequest.c2bConfirmation();
        
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    @RequestMapping(value = "/c2b/validate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateC2BRequest(HttpServletRequest httpRequest, @RequestBody String response) {
       String ipAddress = httpRequest.getRemoteAddr();
        
        log.log(Level.INFO, "VALIDATION " + response + " " + ipAddress );
   
        // http client to be exposed to make transaction 
        
        String responseBody = mpesaRequest.c2bValidate();
        
        //send repquest
        
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/lipanampesa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> lipanampesa(HttpServletRequest httpRequest, @RequestBody LNMRequest request){
         /**
          * receives lipa na mpesa online transaction request
          * check the validity of params
          * saves the request and response from Safaricom
          * sends response from safaricom back to the sender
          */
         
         String ipAddress = httpRequest.getRemoteAddr();
         
         String sandboxUrl = mpesaConfig.getLnmSandBoxUrl();
         
         String phoneNumber = request.getPhoneNumber();
         Double amount = request.getAmount();
        
         JSONObject res = new JSONObject();
         String response = null;
         
         /**
          * check for nullity
          */
         if (PaymentUtils.isNullOrEmpty(request.getPhoneNumber()) || request.getAmount() == null 
            || PaymentUtils.isNullOrEmpty(request.getAccRef())|| PaymentUtils.isNullOrEmpty(request.getTransactionDesc()) 
            ||  PaymentUtils.isNullOrEmpty(request.getResultUrl())){
            
            response = "Some parameter is missing";
            res.put("response", response); 
            //Save request to the db
            dataService.lnmMpesaRequestHelper(ipAddress, request, res.toString(), RequestStatus.FAIL);
            //return response
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
             
         }
         
         //check if the accref is unique
         List<LnmMpesaRequest> lnmMpesaRequests = dataService.findLnmMpesaRequestByAccountRef(request.getAccRef());
         log.log(Level.INFO, String.valueOf(lnmMpesaRequests.size()));
         
         int listSize = lnmMpesaRequests.size();
         if( listSize > 0){
             
            response = "Account reference number already exists";
            res.put("response", response); 
            
            String dupAccRef = request.getAccRef() + "dup";
            request.setAccRef(dupAccRef);
            
            dataService.lnmMpesaRequestHelper(ipAddress, request, res.toString(), RequestStatus.FAIL);
            
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
         } 
         /**
          * check for validity of phone number
         */
         if (PaymentUtils.isValidPhoneNumber(phoneNumber)) {
             String requestBody = mpesaRequest.lipaNaMpesaRequest(request);
             
             response = mpesaService.sendRequest(sandboxUrl, requestBody);
             //Save request to the db
             dataService.lnmMpesaRequestHelper(ipAddress, request, response, RequestStatus.SUCCESS);
             //return response
             return new ResponseEntity<>(response, HttpStatus.OK);
         }else{
             response = "Invalid phone number";
             res.put("response", response);
             //Save request to the db
             dataService.lnmMpesaRequestHelper(ipAddress, request, res.toString(), RequestStatus.FAIL);
             //return response
             return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
         }
    }
    
    @RequestMapping(value = "/lipanampesa/callback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void lipanampesaCallback(HttpServletRequest httpRequest, @RequestBody String response) {
        log.log(Level.INFO, response);
        
        String ipAddress = httpRequest.getRemoteAddr();
        

        dataService.mapToLnmMpesaTransaction(response,ipAddress);
        
        //initiate producer to send message to respective application
        mpesaResponseSender.sendMessage(MpesaTransactionType.LNM_TRANS, response);
    }
    
    
    @RequestMapping(value = "/b2b/request", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> b2bPaymentRequest(HttpServletRequest httpRequest, @RequestBody  B2BRequest b2BRequest){
        /**
         * Receives B2b payment request
         * process request and send
         * persist request
         * send response to originator
         */
     
        String ipAddress = httpRequest.getRemoteAddr();
        
        String sandboxUrl = mpesaConfig.getB2bSandBoxUrl();
        
        JSONObject res = new JSONObject();
        String response = null;
        
        /**
          * check for nulls
         */
        if(PaymentUtils.isNullOrEmpty(b2BRequest.getShortCode()) || PaymentUtils.isNullOrEmpty(b2BRequest.getAccRef())
           || b2BRequest.getAmount() == null || PaymentUtils.isNullOrEmpty(b2BRequest.getRemarks())){
           
           response = "Some parameter is missing";
           res.put("response", response);
           dataService.b2bMpesaRequestHelper(ipAddress, b2BRequest, res.toString(), RequestStatus.FAIL);
           return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
        }
        
        
         List<B2bMpesaRequest> b2bMpesaRequest = dataService.findB2bMpesaRequestByAccountRef(b2BRequest.getAccRef());
         int listSize = b2bMpesaRequest.size();
         if( listSize > 0){
             
            response = "Account reference number already exists";
            res.put("response", response); 
            
            String dupAccRef = b2BRequest.getAccRef() + "dup";
            b2BRequest.setAccRef(dupAccRef);
            
            dataService.b2bMpesaRequestHelper(ipAddress, b2BRequest, res.toString(), RequestStatus.FAIL);
            
            return new ResponseEntity<>(res.toString(), HttpStatus.BAD_REQUEST);
         } 
         
        
         String requestBody = mpesaRequest.b2bPaymentRequest(b2BRequest);
         response = mpesaService.sendRequest(sandboxUrl, requestBody);
         dataService.b2bMpesaRequestHelper(ipAddress, b2BRequest, response, RequestStatus.SUCCESS);
         return new ResponseEntity<>(response, HttpStatus.OK);
        
    }
    
    @RequestMapping(value = "/b2b/queueTimeOut", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void B2BQueueTimeout(HttpServletRequest httpRequest, @RequestBody String response) {
        String ipAddress = httpRequest.getRemoteAddr();

        log.log(Level.INFO, ">>>>##### QUEUE TIMEOUT {0}", ipAddress + response);
        //send response to owner
    }
    
    @RequestMapping(value = "/b2b/result", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void B2BResultURL(HttpServletRequest httpRequest, @RequestBody String response) {
        String ipAddress = httpRequest.getRemoteAddr();
        log.log(Level.INFO, ">>>>##### RESULT URL {0}",ipAddress + response);

        dataService.mapToB2bTransaction(response,ipAddress);
        
        //return response to request initializer
        mpesaResponseSender.sendMessage(MpesaTransactionType.B2B_TRANS, response);
    }
    
}
