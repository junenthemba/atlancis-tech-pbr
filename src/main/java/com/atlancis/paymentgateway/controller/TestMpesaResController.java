/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nthemba
 */

@RestController
@RequestMapping("/mpesa/res")
public class TestMpesaResController {
    
    private Logger log = Logger.getLogger(TestMpesaResController.class.getName());
    
    @RequestMapping(value = "/lipanampesa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void getLnmResponse(@RequestBody String response){
        log.log(Level.INFO, response);
    }
    
    @RequestMapping(value = "/b2b", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void getB2bResponse(@RequestBody String response){
        log.log(Level.INFO, response);
    }
    
}
