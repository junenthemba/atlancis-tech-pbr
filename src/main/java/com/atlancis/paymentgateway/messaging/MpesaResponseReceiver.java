/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.messaging;

import com.atlancis.paymentgateway.configuration.MessagingConfig;
import com.atlancis.paymentgateway.model.B2bMpesaRequest;
import com.atlancis.paymentgateway.model.LnmMpesaRequest;
import com.atlancis.paymentgateway.service.dataservice.DataService;
import com.atlancis.paymentgateway.service.mpesaService.MpesaApiService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nthemba
 */
@Service
public class MpesaResponseReceiver {

    private Logger log = Logger.getLogger(MpesaResponseReceiver.class.getName());

    @Autowired
    DataService dataService;

    @Autowired
    MpesaApiService mpesaApiService;

    @RabbitListener(queues = MessagingConfig.QUEUE_RESPONSE)
    public void receiveMessage(final Message message) {
        log.log(Level.INFO, "Received message as generic: {}" + message.toString());

        String text = new String(message.getBody());

        log.log(Level.INFO, "Received message body " + text);

//        getResultUrl(text);
        JSONObject params = new JSONObject(text);

        String transactionType = params.optString("transactionType");
        JSONObject mpesaResponse = new JSONObject(params.optString("response"));

        switch (transactionType) {
            case "LNM_TRANS":
                sendLnmMpesaResponse(mpesaResponse);
                break;
           
            case "B2B_TRANS":
                sendB2bMpesaResponse(mpesaResponse);
                
        }
    }

    private void sendLnmMpesaResponse(JSONObject mpesaResponse) {
        JSONObject transactionDetail = mpesaResponse.optJSONObject("Body").optJSONObject("stkCallback");

        String merchantRequestId = transactionDetail.optString("MerchantRequestID");
        String checkoutRequestId = transactionDetail.optString("CheckoutRequestID");

        LnmMpesaRequest lnmMpesaRequest = new LnmMpesaRequest();

        try{
            lnmMpesaRequest = dataService.getLnmMpesaRequestByMerchantAndCheckoutId(merchantRequestId, checkoutRequestId);
        }catch(Exception e){
            log.log(Level.SEVERE, "Failed to Query lipa na mpesa request " + e.getMessage());
            e.printStackTrace();
        }
        
        String resultUrl = lnmMpesaRequest.getResultUrl();
        mpesaApiService.sendMpesaResponse(resultUrl, mpesaResponse.toString());
           
    }
    
    private void sendB2bMpesaResponse(JSONObject mpesaResponse) {
        
        JSONObject result = mpesaResponse.optJSONObject("Result");
        
        String originatorConversationID = result.optString("OriginatorConversationID");
        String conversationID = result.optString("ConversationID");
        
        B2bMpesaRequest B2bMpesaRequest = new B2bMpesaRequest();
        
        try{
            B2bMpesaRequest = dataService.findB2bMpesaRequestByOriginatorConversationAndConversationID(originatorConversationID, conversationID);
        }catch(Exception e){
            log.log(Level.SEVERE, "Failed to query B2b mpesa request " + mpesaResponse.toString() + e.getMessage());
            e.printStackTrace();
        }
        
        String resultUrl = B2bMpesaRequest.getResultUrl();
        mpesaApiService.sendMpesaResponse(resultUrl, mpesaResponse.toString());
    }

//    private void getResultUrl(String text) {
//        JSONObject params = new JSONObject(text);
//
//        String transactionType = params.optString("transactionType");
//        JSONObject mpesaResponse = new JSONObject(params.optString("response"));
//
//        String resultUrl = null;
//
//        switch (transactionType) {
//            case "LNM_TRANS":
//                resultUrl = getLNMResultUrl(mpesaResponse);
//                mpesaApiService.sendMpesaResponse(resultUrl, mpesaResponse.toString());
//                break;
//
//            default:
//                log.log(Level.SEVERE, "Transaction type " + transactionType + " does not exist.");
//
//        }
//
//    }

//    private String getLNMResultUrl(JSONObject mpesaResponse) {
//        JSONObject transactionDetail = mpesaResponse.optJSONObject("Body").optJSONObject("stkCallback");
//
//        String merchantRequestId = transactionDetail.optString("MerchantRequestID");
//        String checkoutRequestId = transactionDetail.optString("CheckoutRequestID");
//
//        String resultUrl = dataService.getResultUrl(merchantRequestId, checkoutRequestId);
//        log.log(Level.INFO, resultUrl);
//
//        return resultUrl;
//
//    }

    

}
