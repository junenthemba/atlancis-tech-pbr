/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.messaging;

import com.atlancis.paymentgateway.configuration.MessagingConfig;
import com.atlancis.paymentgateway.service.mpesaService.MpesaTransactionType;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author nthemba
 */
@Service
public class MpesaResponseSender {
    
     private Logger log =  Logger.getLogger(MpesaResponseSender.class.getName());
     
     private final RabbitTemplate rabbitTemplate;
  
     public MpesaResponseSender(final RabbitTemplate rabbitTemplate){
         this.rabbitTemplate = rabbitTemplate;
     }
     
        public void sendMessage(MpesaTransactionType command,String source) {
        JSONObject params = new JSONObject();
        params.put("response", source);
        params.put("transactionType", command);
       
        final String message = params.toString();
        log.info("Sending Mpesa response message... " + message);
        
        try {
            rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE_NAME, MessagingConfig.ROUTING_KEY_RESPONSE, message);
            log.log(Level.INFO, "Response is being processed before being sent");
        }catch(Exception e){
            //when the producer fails to send mpesa response how do I note that?
            log.log(Level.SEVERE, "Failed to send Mpesa message");
            e.printStackTrace();
        }
        
        String response = "Request has been received and ready for processing";
        
    }
}
