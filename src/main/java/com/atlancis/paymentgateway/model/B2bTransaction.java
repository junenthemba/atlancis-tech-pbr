/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author nthemba
 */
@Entity
@Table(name = "b2b_transaction")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "B2bTransaction.findAll", query = "SELECT b FROM B2bTransaction b"),
    @NamedQuery(name = "B2bTransaction.findById", query = "SELECT b FROM B2bTransaction b WHERE b.id = :id"),
    @NamedQuery(name = "B2bTransaction.findByIpAddress", query = "SELECT b FROM B2bTransaction b WHERE b.ipAddress = :ipAddress"),
    @NamedQuery(name = "B2bTransaction.findByResultType", query = "SELECT b FROM B2bTransaction b WHERE b.resultType = :resultType"),
    @NamedQuery(name = "B2bTransaction.findByResultCode", query = "SELECT b FROM B2bTransaction b WHERE b.resultCode = :resultCode"),
    @NamedQuery(name = "B2bTransaction.findByResultDesc", query = "SELECT b FROM B2bTransaction b WHERE b.resultDesc = :resultDesc"),
    @NamedQuery(name = "B2bTransaction.findByOriginatorConversationID", query = "SELECT b FROM B2bTransaction b WHERE b.originatorConversationID = :originatorConversationID"),
    @NamedQuery(name = "B2bTransaction.findByConversationID", query = "SELECT b FROM B2bTransaction b WHERE b.conversationID = :conversationID"),
    @NamedQuery(name = "B2bTransaction.findByTransactionID", query = "SELECT b FROM B2bTransaction b WHERE b.transactionID = :transactionID"),
    @NamedQuery(name = "B2bTransaction.findByDebitAccountBalance", query = "SELECT b FROM B2bTransaction b WHERE b.debitAccountBalance = :debitAccountBalance"),
    @NamedQuery(name = "B2bTransaction.findByInitiatorAccountCurrentBalance", query = "SELECT b FROM B2bTransaction b WHERE b.initiatorAccountCurrentBalance = :initiatorAccountCurrentBalance"),
    @NamedQuery(name = "B2bTransaction.findByDebitAccountCurrentBalance", query = "SELECT b FROM B2bTransaction b WHERE b.debitAccountCurrentBalance = :debitAccountCurrentBalance"),
    @NamedQuery(name = "B2bTransaction.findByAmount", query = "SELECT b FROM B2bTransaction b WHERE b.amount = :amount"),
    @NamedQuery(name = "B2bTransaction.findByTransCompletedTime", query = "SELECT b FROM B2bTransaction b WHERE b.transCompletedTime = :transCompletedTime"),
    @NamedQuery(name = "B2bTransaction.findByDebitPartyCharges", query = "SELECT b FROM B2bTransaction b WHERE b.debitPartyCharges = :debitPartyCharges"),
    @NamedQuery(name = "B2bTransaction.findByCreditPartyPublicName", query = "SELECT b FROM B2bTransaction b WHERE b.creditPartyPublicName = :creditPartyPublicName"),
    @NamedQuery(name = "B2bTransaction.findByDebitPartyPublicName", query = "SELECT b FROM B2bTransaction b WHERE b.debitPartyPublicName = :debitPartyPublicName"),
    @NamedQuery(name = "B2bTransaction.findByCreditAccountBalance", query = "SELECT b FROM B2bTransaction b WHERE b.creditAccountBalance = :creditAccountBalance"),
    @NamedQuery(name = "B2bTransaction.findByCreatedAt", query = "SELECT b FROM B2bTransaction b WHERE b.createdAt = :createdAt"),
    @NamedQuery(name = "B2bTransaction.findByUpdatedAt", query = "SELECT b FROM B2bTransaction b WHERE b.updatedAt = :updatedAt")})
public class B2bTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ipAddress")
    private String ipAddress;
    @Basic(optional = false)
    @NotNull
    @Column(name = "resultType")
    private int resultType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "resultCode")
    private int resultCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "resultDesc")
    private String resultDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "originatorConversationID")
    private String originatorConversationID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "conversationID")
    private String conversationID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "transactionID")
    private String transactionID;
    @Size(max = 200)
    @Column(name = "debitAccountBalance")
    private String debitAccountBalance;
    @Size(max = 200)
    @Column(name = "initiatorAccountCurrentBalance")
    private String initiatorAccountCurrentBalance;
    @Size(max = 200)
    @Column(name = "debitAccountCurrentBalance")
    private String debitAccountCurrentBalance;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Column(name = "transCompletedTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transCompletedTime;
    @Size(max = 200)
    @Column(name = "debitPartyCharges")
    private String debitPartyCharges;
    @Size(max = 200)
    @Column(name = "creditPartyPublicName")
    private String creditPartyPublicName;
    @Size(max = 200)
    @Column(name = "debitPartyPublicName")
    private String debitPartyPublicName;
    @Size(max = 200)
    @Column(name = "creditAccountBalance")
    private String creditAccountBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public B2bTransaction() {
    }

    public B2bTransaction(Long id) {
        this.id = id;
    }

    public B2bTransaction(Long id, String ipAddress, int resultType, int resultCode, String resultDesc, String originatorConversationID, String conversationID, String transactionID, Date createdAt, Date updatedAt) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.resultType = resultType;
        this.resultCode = resultCode;
        this.resultDesc = resultDesc;
        this.originatorConversationID = originatorConversationID;
        this.conversationID = conversationID;
        this.transactionID = transactionID;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultDesc() {
        return resultDesc;
    }

    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

    public String getOriginatorConversationID() {
        return originatorConversationID;
    }

    public void setOriginatorConversationID(String originatorConversationID) {
        this.originatorConversationID = originatorConversationID;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getDebitAccountBalance() {
        return debitAccountBalance;
    }

    public void setDebitAccountBalance(String debitAccountBalance) {
        this.debitAccountBalance = debitAccountBalance;
    }

    public String getInitiatorAccountCurrentBalance() {
        return initiatorAccountCurrentBalance;
    }

    public void setInitiatorAccountCurrentBalance(String initiatorAccountCurrentBalance) {
        this.initiatorAccountCurrentBalance = initiatorAccountCurrentBalance;
    }

    public String getDebitAccountCurrentBalance() {
        return debitAccountCurrentBalance;
    }

    public void setDebitAccountCurrentBalance(String debitAccountCurrentBalance) {
        this.debitAccountCurrentBalance = debitAccountCurrentBalance;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getTransCompletedTime() {
        return transCompletedTime;
    }

    public void setTransCompletedTime(Date transCompletedTime) {
        this.transCompletedTime = transCompletedTime;
    }

    public String getDebitPartyCharges() {
        return debitPartyCharges;
    }

    public void setDebitPartyCharges(String debitPartyCharges) {
        this.debitPartyCharges = debitPartyCharges;
    }

    public String getCreditPartyPublicName() {
        return creditPartyPublicName;
    }

    public void setCreditPartyPublicName(String creditPartyPublicName) {
        this.creditPartyPublicName = creditPartyPublicName;
    }

    public String getDebitPartyPublicName() {
        return debitPartyPublicName;
    }

    public void setDebitPartyPublicName(String debitPartyPublicName) {
        this.debitPartyPublicName = debitPartyPublicName;
    }

    public String getCreditAccountBalance() {
        return creditAccountBalance;
    }

    public void setCreditAccountBalance(String creditAccountBalance) {
        this.creditAccountBalance = creditAccountBalance;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof B2bTransaction)) {
            return false;
        }
        B2bTransaction other = (B2bTransaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.atlancis.paymentgateway.model.B2bTransaction[ id=" + id + " ]";
    }
    
}
