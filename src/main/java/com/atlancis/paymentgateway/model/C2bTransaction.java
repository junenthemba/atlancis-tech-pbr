/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author nthemba
 */
@Entity
@Table(name = "c2b_transaction")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "C2bTransaction.findAll", query = "SELECT c FROM C2bTransaction c"),
    @NamedQuery(name = "C2bTransaction.findById", query = "SELECT c FROM C2bTransaction c WHERE c.id = :id"),
    @NamedQuery(name = "C2bTransaction.findByIpAddress", query = "SELECT c FROM C2bTransaction c WHERE c.ipAddress = :ipAddress"),
    @NamedQuery(name = "C2bTransaction.findByTransactionType", query = "SELECT c FROM C2bTransaction c WHERE c.transactionType = :transactionType"),
    @NamedQuery(name = "C2bTransaction.findByTransID", query = "SELECT c FROM C2bTransaction c WHERE c.transID = :transID"),
    @NamedQuery(name = "C2bTransaction.findByFirstName", query = "SELECT c FROM C2bTransaction c WHERE c.firstName = :firstName"),
    @NamedQuery(name = "C2bTransaction.findByMiddleName", query = "SELECT c FROM C2bTransaction c WHERE c.middleName = :middleName"),
    @NamedQuery(name = "C2bTransaction.findByLastName", query = "SELECT c FROM C2bTransaction c WHERE c.lastName = :lastName"),
    @NamedQuery(name = "C2bTransaction.findByMsisdn", query = "SELECT c FROM C2bTransaction c WHERE c.msisdn = :msisdn"),
    @NamedQuery(name = "C2bTransaction.findByTransAmount", query = "SELECT c FROM C2bTransaction c WHERE c.transAmount = :transAmount"),
    @NamedQuery(name = "C2bTransaction.findByTransTime", query = "SELECT c FROM C2bTransaction c WHERE c.transTime = :transTime"),
    @NamedQuery(name = "C2bTransaction.findByOrgAccountBalance", query = "SELECT c FROM C2bTransaction c WHERE c.orgAccountBalance = :orgAccountBalance"),
    @NamedQuery(name = "C2bTransaction.findByBusinessShortCode", query = "SELECT c FROM C2bTransaction c WHERE c.businessShortCode = :businessShortCode"),
    @NamedQuery(name = "C2bTransaction.findByBillRefNumber", query = "SELECT c FROM C2bTransaction c WHERE c.billRefNumber = :billRefNumber"),
    @NamedQuery(name = "C2bTransaction.findByInvoiceNumber", query = "SELECT c FROM C2bTransaction c WHERE c.invoiceNumber = :invoiceNumber"),
    @NamedQuery(name = "C2bTransaction.findByThirdPartyTransID", query = "SELECT c FROM C2bTransaction c WHERE c.thirdPartyTransID = :thirdPartyTransID"),
    @NamedQuery(name = "C2bTransaction.findByCreatedAt", query = "SELECT c FROM C2bTransaction c WHERE c.createdAt = :createdAt"),
    @NamedQuery(name = "C2bTransaction.findByUpdatedAt", query = "SELECT c FROM C2bTransaction c WHERE c.updatedAt = :updatedAt")})
public class C2bTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ipAddress")
    private String ipAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TransactionType")
    private String transactionType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TransID")
    private String transID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "FirstName")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "MiddleName")
    private String middleName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "LastName")
    private String lastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "MSISDN")
    private String msisdn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TransAmount")
    private double transAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TransTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OrgAccountBalance")
    private double orgAccountBalance;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "BusinessShortCode")
    private String businessShortCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BillRefNumber")
    private String billRefNumber;
    @Size(max = 100)
    @Column(name = "InvoiceNumber")
    private String invoiceNumber;
    @Size(max = 100)
    @Column(name = "ThirdPartyTransID")
    private String thirdPartyTransID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
    @Column(name = "updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public C2bTransaction() {
    }

    public C2bTransaction(Long id) {
        this.id = id;
    }

    public C2bTransaction(Long id, String ipAddress, String transactionType, String transID, String firstName, String middleName, String lastName, String msisdn, double transAmount, Date transTime, double orgAccountBalance, String businessShortCode, String billRefNumber, Date createdAt) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.transactionType = transactionType;
        this.transID = transID;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.msisdn = msisdn;
        this.transAmount = transAmount;
        this.transTime = transTime;
        this.orgAccountBalance = orgAccountBalance;
        this.businessShortCode = businessShortCode;
        this.billRefNumber = billRefNumber;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransID() {
        return transID;
    }

    public void setTransID(String transID) {
        this.transID = transID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public double getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(double transAmount) {
        this.transAmount = transAmount;
    }

    public Date getTransTime() {
        return transTime;
    }

    public void setTransTime(Date transTime) {
        this.transTime = transTime;
    }

    public double getOrgAccountBalance() {
        return orgAccountBalance;
    }

    public void setOrgAccountBalance(double orgAccountBalance) {
        this.orgAccountBalance = orgAccountBalance;
    }

    public String getBusinessShortCode() {
        return businessShortCode;
    }

    public void setBusinessShortCode(String businessShortCode) {
        this.businessShortCode = businessShortCode;
    }

    public String getBillRefNumber() {
        return billRefNumber;
    }

    public void setBillRefNumber(String billRefNumber) {
        this.billRefNumber = billRefNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getThirdPartyTransID() {
        return thirdPartyTransID;
    }

    public void setThirdPartyTransID(String thirdPartyTransID) {
        this.thirdPartyTransID = thirdPartyTransID;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof C2bTransaction)) {
            return false;
        }
        C2bTransaction other = (C2bTransaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.atlancis.paymentgateway.model.C2bTransaction[ id=" + id + " ]";
    }
    
}
