/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author nthemba
 */
@Entity
@Table(name = "lnm_mpesa_request")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LnmMpesaRequest.findAll", query = "SELECT l FROM LnmMpesaRequest l"),
    @NamedQuery(name = "LnmMpesaRequest.findById", query = "SELECT l FROM LnmMpesaRequest l WHERE l.id = :id"),
    @NamedQuery(name = "LnmMpesaRequest.findByIpAddress", query = "SELECT l FROM LnmMpesaRequest l WHERE l.ipAddress = :ipAddress"),
    @NamedQuery(name = "LnmMpesaRequest.findByPhoneNumber", query = "SELECT l FROM LnmMpesaRequest l WHERE l.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "LnmMpesaRequest.findByAmount", query = "SELECT l FROM LnmMpesaRequest l WHERE l.amount = :amount"),
    @NamedQuery(name = "LnmMpesaRequest.findByAccountRef", query = "SELECT l FROM LnmMpesaRequest l WHERE l.accountRef = :accountRef"),
    @NamedQuery(name = "LnmMpesaRequest.findByTransactionDesc", query = "SELECT l FROM LnmMpesaRequest l WHERE l.transactionDesc = :transactionDesc"),
    @NamedQuery(name = "LnmMpesaRequest.findByResultUrl", query = "SELECT l FROM LnmMpesaRequest l WHERE l.resultUrl = :resultUrl"),
    @NamedQuery(name = "LnmMpesaRequest.findByRequestStatus", query = "SELECT l FROM LnmMpesaRequest l WHERE l.requestStatus = :requestStatus"),
    @NamedQuery(name = "LnmMpesaRequest.findByMerchantRequestID", query = "SELECT l FROM LnmMpesaRequest l WHERE l.merchantRequestID = :merchantRequestID"),
    @NamedQuery(name = "LnmMpesaRequest.findByCheckoutRequestID", query = "SELECT l FROM LnmMpesaRequest l WHERE l.checkoutRequestID = :checkoutRequestID"),
    @NamedQuery(name = "LnmMpesaRequest.findByResponseDescription", query = "SELECT l FROM LnmMpesaRequest l WHERE l.responseDescription = :responseDescription"),
    @NamedQuery(name = "LnmMpesaRequest.findByResponseCode", query = "SELECT l FROM LnmMpesaRequest l WHERE l.responseCode = :responseCode"),
    @NamedQuery(name = "LnmMpesaRequest.findByCustomerMessage", query = "SELECT l FROM LnmMpesaRequest l WHERE l.customerMessage = :customerMessage"),
    @NamedQuery(name = "LnmMpesaRequest.findByCreatedAt", query = "SELECT l FROM LnmMpesaRequest l WHERE l.createdAt = :createdAt"),
    @NamedQuery(name = "LnmMpesaRequest.findByUpdatedAt", query = "SELECT l FROM LnmMpesaRequest l WHERE l.updatedAt = :updatedAt")})
public class LnmMpesaRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ipAddress")
    private String ipAddress;
    @Size(max = 200)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Size(max = 200)
    @Column(name = "accountRef")
    private String accountRef;
    @Size(max = 200)
    @Column(name = "transactionDesc")
    private String transactionDesc;
    @Size(max = 200)
    @Column(name = "resultUrl")
    private String resultUrl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "requestStatus")
    private String requestStatus;
    @Size(max = 200)
    @Column(name = "merchantRequestID")
    private String merchantRequestID;
    @Size(max = 200)
    @Column(name = "checkoutRequestID")
    private String checkoutRequestID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "responseDescription")
    private String responseDescription;
    @Column(name = "responseCode")
    private Integer responseCode;
    @Size(max = 200)
    @Column(name = "customerMessage")
    private String customerMessage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public LnmMpesaRequest() {
    }

    public LnmMpesaRequest(Long id) {
        this.id = id;
    }

    public LnmMpesaRequest(Long id, String ipAddress, String requestStatus, String responseDescription, Date createdAt, Date updatedAt) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.requestStatus = requestStatus;
        this.responseDescription = responseDescription;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(String accountRef) {
        this.accountRef = accountRef;
    }

    public String getTransactionDesc() {
        return transactionDesc;
    }

    public void setTransactionDesc(String transactionDesc) {
        this.transactionDesc = transactionDesc;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getMerchantRequestID() {
        return merchantRequestID;
    }

    public void setMerchantRequestID(String merchantRequestID) {
        this.merchantRequestID = merchantRequestID;
    }

    public String getCheckoutRequestID() {
        return checkoutRequestID;
    }

    public void setCheckoutRequestID(String checkoutRequestID) {
        this.checkoutRequestID = checkoutRequestID;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getCustomerMessage() {
        return customerMessage;
    }

    public void setCustomerMessage(String customerMessage) {
        this.customerMessage = customerMessage;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LnmMpesaRequest)) {
            return false;
        }
        LnmMpesaRequest other = (LnmMpesaRequest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.atlancis.paymentgateway.model.LnmMpesaRequest[ id=" + id + " ]";
    }
    
}
