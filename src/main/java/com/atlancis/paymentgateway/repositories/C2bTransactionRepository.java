/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.repositories;

import com.atlancis.paymentgateway.model.C2bTransaction;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nthemba
 */
@Repository
public interface C2bTransactionRepository  extends JpaRepository<C2bTransaction, Long>{
    
    /**
     *
     * @param TransID
     * @return
     */
    @Query("SELECT c FROM C2bTransaction c where c.transID = :TransID") 
    C2bTransaction findByTransID(@Param("TransID") String TransID);
    
    List<C2bTransaction> findByMsisdn(String msisdn);
    
}
