/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.dataservice;

import com.atlancis.paymentgateway.customEntitites.B2BRequest;
import com.atlancis.paymentgateway.customEntitites.LNMRequest;
import com.atlancis.paymentgateway.model.B2bMpesaRequest;
import com.atlancis.paymentgateway.model.B2bTransaction;
import com.atlancis.paymentgateway.model.LnmMpesaTransaction;
import com.atlancis.paymentgateway.model.C2bTransaction;
import com.atlancis.paymentgateway.model.LnmMpesaRequest;
import com.atlancis.paymentgateway.model.MpesaRequest;
import com.atlancis.paymentgateway.service.mpesaService.MpesaTransactionType;
import com.atlancis.paymentgateway.service.mpesaService.RequestStatus;
import com.atlancis.paymentgateway.controller.MpesaApiController;
import com.atlancis.paymentgateway.customEntitites.C2bResponse;
import com.atlancis.paymentgateway.repositories.B2bMpesaRequestRepository;
import com.atlancis.paymentgateway.repositories.B2bTransactionRepository;
import com.atlancis.paymentgateway.repositories.C2bTransactionRepository;
import com.atlancis.paymentgateway.repositories.MpesaRequestRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.atlancis.paymentgateway.utils.PaymentUtils;
import com.atlancis.paymentgateway.repositories.LnmMpesaTransactionRepository;
import com.atlancis.paymentgateway.repositories.LnmMpesaRequestRepository;

/**
 *
 * @author nthemba
 */
@Transactional
@Service
public class DataServiceImpl implements DataService{
    
    private Logger log = Logger.getLogger(MpesaApiController.class.getName());
    
    @Autowired
    LnmMpesaTransactionRepository lnmMpesaTransactionRepo;
    
    @Autowired
    C2bTransactionRepository c2bTransactionRepo;
    
    @Autowired
    B2bTransactionRepository b2bTransactionRepository;
    
    @Autowired
    MpesaRequestRepository mpesaRequestRepository;
    
    @Autowired
    LnmMpesaRequestRepository lnmMpesaRequestRepository;
    
    @Autowired
    B2bMpesaRequestRepository b2bMpesaRequestRepository;
    
    
    
    @Override
    public C2bTransaction saveC2BTransaction(C2bTransaction c2bTransaction) {
       return c2bTransactionRepo.save(c2bTransaction);
    }

    @Override
    public List<C2bTransaction> listC2bTransaction() {
        return c2bTransactionRepo.findAll();
    }

    @Override
    public C2bTransaction getC2bTransactionByTransId(String TransID) {
        return c2bTransactionRepo.findByTransID(TransID);
    }

    @Override
    public List<C2bTransaction> getC2bTransactionByPhoneNumber(String phoneNumber) {
        return c2bTransactionRepo.findByMsisdn(phoneNumber);
    }

    @Override
    public LnmMpesaTransaction saveLnmMpesaTransaction(LnmMpesaTransaction lnmMpesaTransaction) {
        return lnmMpesaTransactionRepo.save(lnmMpesaTransaction);
    }

    @Override
    public List<LnmMpesaTransaction> listLnmMpesaTransaction() {
        return lnmMpesaTransactionRepo.findAll();
    }

    @Override
    public LnmMpesaTransaction getLnmMpesaTransactionByTransId(String transId) {
        return lnmMpesaTransactionRepo.findByMpesaReceiptNumber(transId);
    }

    @Override
    public List<LnmMpesaTransaction> getLnmMpesaTransactionByPhoneNumber(String phoneNumber) {
        return lnmMpesaTransactionRepo.findByPhoneNumber(phoneNumber);
    }
    
    @Override
    public B2bTransaction saveB2bTransaction(B2bTransaction b2bTransaction) {
        return b2bTransactionRepository.save(b2bTransaction);
    }

    @Override
    public List<B2bTransaction> listB2bTransaction() {
        return b2bTransactionRepository.findAll();
    }

    @Override
    public B2bTransaction getB2bTransactionByTransactionId(String transactionID) {
        return b2bTransactionRepository.findByTransactionID(transactionID);
    }

    @Override
    public void mapToC2bTransaction(String response) {

        ObjectMapper objectMapper = new ObjectMapper();
        C2bTransaction c2b = new C2bTransaction();

        try {
            //map response to respective class 
            c2b = objectMapper.readValue(response, C2bTransaction.class);
            log.log(Level.INFO, "C2b mapped to class");
            
            //check if transaction exists
            if (getC2bTransactionByTransId(c2b.getTransID()) == null) {
                try {
                   //save transation to db
                    saveC2BTransaction(c2b);
                    //create queue to send mpesa response to the respective owner 
                    //post
                    
                    log.log(Level.INFO, "C2B transaction saved to db");
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Failed Saving c2b transaction to db");
                    e.printStackTrace();
                }

            } else {
                log.log(Level.SEVERE, " C2B Transaction already exists");
            }
        } catch (IOException ex) {
            Logger.getLogger(MpesaApiController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    @Override
    public void mapToC2bTransaction(C2bResponse c2bResponse, String ipAddress) {
        C2bTransaction c2b = new C2bTransaction();
        
        c2b.setIpAddress(ipAddress);
        c2b.setTransactionType(c2bResponse.getTransactionType());
        c2b.setTransID(c2bResponse.getTransID());
        c2b.setFirstName(c2bResponse.getFirstName());
        c2b.setMiddleName(c2bResponse.getMiddleName());
        c2b.setLastName(c2bResponse.getLastName());
        c2b.setMsisdn(c2bResponse.getMSISDN());
        c2b.setTransAmount(Double.valueOf(c2bResponse.getTransAmount()));
                
        //Parse String to date
        String transactionDate = c2bResponse.getTransTime();
         Date trxTime = null;
         if (StringUtils.isNotEmpty(transactionDate)) {
             SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMddHHmmss");// 20170826164704
             try {
                 trxTime = dtf.parse(transactionDate);
             } catch (ParseException e) {
                 e.printStackTrace();
             }
        }
         
        c2b.setTransTime(trxTime);
        c2b.setOrgAccountBalance(Double.valueOf(c2bResponse.getOrgAccountBalance()));
        c2b.setBusinessShortCode(c2bResponse.getBusinessShortCode());
        c2b.setBillRefNumber(c2bResponse.getBillRefNumber());
        c2b.setThirdPartyTransID(c2bResponse.getThirdPartyTransID());
        c2b.setInvoiceNumber(c2bResponse.getInvoiceNumber());
        
         if (getC2bTransactionByTransId(c2b.getTransID()) == null) {
             try {
                 //save transation to db
                 saveC2BTransaction(c2b);
                 //create queue to send mpesa response to the respective owner 
                 //post
                 log.log(Level.INFO, "C2B transaction saved to db");
             } catch (Exception e) {
                 log.log(Level.SEVERE, "Failed Saving c2b transaction to db");
                 e.printStackTrace();
             }

         } else {
             log.log(Level.SEVERE, " C2B Transaction already exists");
         }
        
    }

    @Override
    public void mapToLnmMpesaTransaction(String response, String ipAddress) {
        //Get the response of lipa na mpesa online and save it to the db
        LnmMpesaTransaction lnmMpesaTransaction = new LnmMpesaTransaction();
        
        JSONObject json = new JSONObject(response);
        
        JSONObject transactionDetail = json.optJSONObject("Body").optJSONObject("stkCallback");

        int resultCode = transactionDetail.optInt("ResultCode");
        log.log(Level.INFO, "Lipa na mpesa result code is " + resultCode);
        
        Map<String, String> map = new HashMap<>();
        //if transaction was successful
        if (resultCode == 0) {
            JSONArray item = json.optJSONObject("Body").optJSONObject("stkCallback").optJSONObject("CallbackMetadata").optJSONArray("Item");

            for (int i = 0; i < item.length(); i++) {
                map.put(item.optJSONObject(i).optString("Name"), item.optJSONObject(i).optString("Value"));
            }

            //Parse String to date
            String transactionDate = map.get("TransactionDate");
            Date trxTime = null;
            if (StringUtils.isNotEmpty(transactionDate)) {
                SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMddHHmmss");// 20170826164704
                try {
                    trxTime = dtf.parse(transactionDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            lnmMpesaTransaction.setAmount(Double.valueOf(map.get("Amount")));
            lnmMpesaTransaction.setMpesaReceiptNumber(map.get("MpesaReceiptNumber"));

            String accBalance = map.get("Balance");

            if (StringUtils.isEmpty(accBalance)) {
                accBalance = null;
                lnmMpesaTransaction.setBalance(0.00);
            } else {
                lnmMpesaTransaction.setBalance(Double.valueOf(accBalance));
            }

            lnmMpesaTransaction.setIpAddress(ipAddress);
            lnmMpesaTransaction.setTransactionDate(trxTime);
            lnmMpesaTransaction.setPhoneNumber(map.get("PhoneNumber"));
            lnmMpesaTransaction.setMerchantRequestID(transactionDetail.optString("MerchantRequestID"));
            lnmMpesaTransaction.setCheckoutRequestID(transactionDetail.optString("CheckoutRequestID"));
            lnmMpesaTransaction.setResultCode(transactionDetail.optInt("ResultCode"));
            lnmMpesaTransaction.setResultDesc(transactionDetail.optString("ResultDesc"));
            
           
            //check if transaction exist
            if(getLnmMpesaTransactionByTransId(map.get("MpesaReceiptNumber")) == null){
                
                try{
                    saveLnmMpesaTransaction(lnmMpesaTransaction);
                }catch(Exception e){
                    log.log(Level.SEVERE, "Failed to save lipa na mpesa transaction {0}", e.getMessage());
                    e.printStackTrace();
                }
            }else{
                log.log(Level.SEVERE, "Transaction already exists");
            }
        
        /**
         * Transaction was not successful
         */
        }else{
            lnmMpesaTransaction.setIpAddress(ipAddress);
            lnmMpesaTransaction.setMerchantRequestID(transactionDetail.optString("MerchantRequestID"));
            lnmMpesaTransaction.setCheckoutRequestID(transactionDetail.optString("CheckoutRequestID"));
            lnmMpesaTransaction.setResultCode(transactionDetail.optInt("ResultCode"));
            lnmMpesaTransaction.setResultDesc(transactionDetail.optString("ResultDesc"));
            
            try {
                saveLnmMpesaTransaction(lnmMpesaTransaction);
            } catch (Exception e) {
                log.log(Level.SEVERE, "Failed to save lipa na mpesa transaction {0}", e.getMessage());
                e.printStackTrace();
            }
        }
  
    }
    
    @Override
    public void mapToB2bTransaction(String response, String ipAddress) {

        B2bTransaction b2bTrans = new B2bTransaction();

        JSONObject json = new JSONObject(response);

        //Set all values in Result
        JSONObject result = json.optJSONObject("Result");

       

        int resultCode = result.optInt("ResultCode");

        if (resultCode == 0) {
            
            JSONArray ResultParams = result.optJSONObject("ResultParameters").optJSONArray("ResultParameter");

            Map<String, String> map = new HashMap();

            for (int i = 0; i < ResultParams.length(); i++) {
                map.put(ResultParams.optJSONObject(i).optString("Key"), ResultParams.optJSONObject(i).optString("Value"));
            }
            
            b2bTrans.setResultType(result.optInt("ResultType"));
            b2bTrans.setResultCode(result.optInt("ResultCode"));
            b2bTrans.setResultDesc(result.optString("ResultDesc"));
            b2bTrans.setOriginatorConversationID(result.optString("OriginatorConversationID"));
            b2bTrans.setConversationID(result.optString("ConversationID"));
            b2bTrans.setTransactionID(result.optString("TransactionID"));
            b2bTrans.setDebitAccountBalance(map.get("DebitAccountBalance"));
            b2bTrans.setInitiatorAccountCurrentBalance(map.get("InitiatorAccountCurrentBalance"));
            b2bTrans.setDebitAccountCurrentBalance(map.get("DebitAccountCurrentBalance"));
            b2bTrans.setAmount(Double.valueOf(map.get("Amount")));
            b2bTrans.setDebitPartyCharges(map.get("DebitPartyCharges"));
            b2bTrans.setCreditPartyPublicName(map.get("CreditPartyPublicName"));
            b2bTrans.setDebitPartyPublicName(map.get("DebitPartyPublicName"));
            b2bTrans.setCreditAccountBalance(map.get("CreditAccountBalance"));

            String transactionDate = map.get("TransCompletedTime");
            Date trxTime = null;
            if (StringUtils.isNotEmpty(transactionDate)) {
                SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMddHHmmss");// 20170826164704
                try {
                    trxTime = dtf.parse(transactionDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            
            b2bTrans.setTransCompletedTime(trxTime);
            b2bTrans.setIpAddress(ipAddress);
   
        } else {
            b2bTrans.setResultType(result.optInt("ResultType"));
            b2bTrans.setResultCode(result.optInt("ResultCode"));
            b2bTrans.setResultDesc(result.optString("ResultDesc"));
            b2bTrans.setOriginatorConversationID(result.optString("OriginatorConversationID"));
            b2bTrans.setConversationID(result.optString("ConversationID"));
            b2bTrans.setTransactionID(result.optString("TransactionID"));
            b2bTrans.setIpAddress(ipAddress);

        }
        
         try {
                saveB2bTransaction(b2bTrans);
                log.log(Level.INFO, "Saved B2b Transaction");
            } catch (Exception e) {
                log.log(Level.SEVERE, "Failed to save B2B transaction " + response + " " + e.getMessage() );
                e.printStackTrace();
            }
        
        

//        if (getB2bTransactionByTransactionId(result.optString("TransactionID")) == null) {
//
//            try {
//                saveB2bTransaction(b2bTrans);
//                log.log(Level.INFO, "Saved B2b Transaction");
//            } catch (Exception e) {
//                log.log(Level.SEVERE, "Failed to save B2B transaction {0}", e.getMessage());
//                e.printStackTrace();
//            }
//        }else{
//            log.log(Level.SEVERE, response);
//        }


          
    }

    @Override
    public void mpesaRequestHelper(String ipAddress, String requestDetails, RequestStatus status, MpesaTransactionType requestType,String requestResult) {
        
        MpesaRequest request =  new MpesaRequest();
        request.setIpAddress(ipAddress);
        request.setRequestDetails(requestDetails);
        request.setRequestStatus(status.name());
        request.setRequestType(requestType.name());
        request.setRequestResult(requestResult);
        
        try{
            saveMpesaRequest(request);
            log.log(Level.INFO, "Saved Mpesa Request");
        }catch(Exception e){
            log.log(Level.SEVERE, "Failed to save mpesa Request");
            e.printStackTrace();
        }
    }

    @Override
    public MpesaRequest saveMpesaRequest(MpesaRequest mpesaRequest) {
       return mpesaRequestRepository.save(mpesaRequest);
    }

    @Override
    public List<MpesaRequest> listMpesaRequest() {
        return mpesaRequestRepository.findAll();
    }

    @Override
    public LnmMpesaRequest saveLnmMpesaRequest(LnmMpesaRequest lnmMpesaRequest) {
        return lnmMpesaRequestRepository.save(lnmMpesaRequest);
    }

    @Override
    public void lnmMpesaRequestHelper(String ipAddress, LNMRequest lnmRequest, String response, RequestStatus requestStatus) {
        
        JSONObject res = new JSONObject(response);
        
        LnmMpesaRequest lnmMpesaRequest = new LnmMpesaRequest();
        
        lnmMpesaRequest.setIpAddress(ipAddress);
        lnmMpesaRequest.setPhoneNumber(lnmRequest.getPhoneNumber());
        lnmMpesaRequest.setAmount(lnmRequest.getAmount());
        lnmMpesaRequest.setAccountRef(lnmRequest.getAccRef());
        lnmMpesaRequest.setTransactionDesc(lnmRequest.getTransactionDesc());
        lnmMpesaRequest.setResultUrl(lnmRequest.getResultUrl());
        lnmMpesaRequest.setRequestStatus(requestStatus.name());
        lnmMpesaRequest.setMerchantRequestID(res.optString("MerchantRequestID"));
        lnmMpesaRequest.setCheckoutRequestID(res.optString("CheckoutRequestID"));
        
        if (PaymentUtils.isNullOrEmpty(res.optString("ResponseDescription"))){
            lnmMpesaRequest.setResponseDescription(res.optString("response"));
        }else{
            lnmMpesaRequest.setResponseDescription(res.optString("ResponseDescription"));
        }
        
        lnmMpesaRequest.setResponseCode(res.optInt("ResponseCode"));
        lnmMpesaRequest.setCustomerMessage(res.optString("CustomerMessage"));
        
        try{
            saveLnmMpesaRequest(lnmMpesaRequest);
        }catch(Exception e){
            log.log(Level.SEVERE, "Failed to save lipa na mpesa online request");
            e.printStackTrace();
        }
    }
    
    @Override
    public List<LnmMpesaRequest> findLnmMpesaRequestByAccountRef(String accountRef) {
        return lnmMpesaRequestRepository.findByAccountRef(accountRef);
    }
    
     @Override
    public String getResultUrl(String merchantRequestId, String checkoutRequestId) {
        return lnmMpesaRequestRepository.findResultUrlByMerchantandCheckoutId(merchantRequestId, checkoutRequestId);
    }
    
    @Override
    public LnmMpesaRequest getLnmMpesaRequestByMerchantAndCheckoutId(String merchantRequestId, String checkoutRequestId) {
        return lnmMpesaRequestRepository.findByMerchantandCheckoutId(merchantRequestId, checkoutRequestId);
    }

    @Override
    public B2bMpesaRequest saveB2bMpesaRequest(B2bMpesaRequest b2bMpesaRequest) {
        return b2bMpesaRequestRepository.save(b2bMpesaRequest);
    }

    @Override
    public void b2bMpesaRequestHelper(String ipAddress, B2BRequest b2BRequest, String response, RequestStatus requestStatus) {
        
        JSONObject res = new JSONObject(response);
        
        B2bMpesaRequest b2bMpesaRequest = new B2bMpesaRequest();
        
        b2bMpesaRequest.setIpAddress(ipAddress);
        b2bMpesaRequest.setShortCode(b2BRequest.getShortCode());
        b2bMpesaRequest.setAmount(b2BRequest.getAmount());
        b2bMpesaRequest.setRemarks(b2BRequest.getRemarks());
        b2bMpesaRequest.setResultUrl(b2BRequest.getResultUrl());
        b2bMpesaRequest.setAccountRef(b2BRequest.getAccRef());
        b2bMpesaRequest.setOriginatorConverstionID(res.optString("OriginatorConversationID"));
        b2bMpesaRequest.setConversationID(res.optString("ConversationID"));
        
        if(PaymentUtils.isNullOrEmpty(res.optString("ResponseDescription"))){
            b2bMpesaRequest.setResponseDescription(res.optString("response"));
        }else{
            b2bMpesaRequest.setResponseDescription(res.optString("ResponseDescription"));
        }
        
        try{
            saveB2bMpesaRequest(b2bMpesaRequest);
        }catch(Exception e){
            log.log(Level.SEVERE, "Failed to save b2bMpesaRequest");
            e.printStackTrace();
        }
        
    }

    @Override
    public List<B2bMpesaRequest> findB2bMpesaRequestByAccountRef(String accountRef) {
        return b2bMpesaRequestRepository.findByAccountRef(accountRef);
    }

    @Override
    public B2bMpesaRequest findB2bMpesaRequestByOriginatorConversationAndConversationID(String originatorConversationID, String conversationID) {
        return b2bMpesaRequestRepository.findByOriginatorConversationAndConversationID(originatorConversationID, conversationID);
    }
   
}
