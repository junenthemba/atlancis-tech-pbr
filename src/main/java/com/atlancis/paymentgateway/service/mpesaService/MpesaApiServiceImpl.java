/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.mpesaService;

import com.atlancis.paymentgateway.configuration.MpesaConfig;
import com.atlancis.paymentgateway.service.dataservice.DataService;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;



/**
 *
 * @author nthemba
 */
@Service
public class MpesaApiServiceImpl implements MpesaApiService{
    
    private Logger log =  Logger.getLogger(MpesaApiServiceImpl.class.getName());
    
     @Autowired MpesaConfig yamlConfig;
     
     @Autowired 
     DataService dataService;
     
    //get mpesa authorization
    public String getAuthorization() throws UnsupportedEncodingException{
        String appKey = yamlConfig.getAppkey();
        String appSecret = yamlConfig.getAppSecret();
        
        String appKeySecret = appKey + ":" + appSecret;
        byte[] bytes = appKeySecret.getBytes("ISO-8859-1");
        String auth = Base64.getEncoder().encodeToString(bytes);
        
        String url = yamlConfig.getAuthSandBoxUrl();
        
        RestTemplate restTemplate =  new RestTemplate();
        
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.add("authorization", "Basic " + auth);
        header.add("cache-control", "no-cache");
        
        HttpEntity<Object> entity = new HttpEntity<>("parameters",header);
        
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        
        log.log(Level.INFO, response.toString());
        
        String authResponse = response.getBody();
        
        JSONObject json = new JSONObject(authResponse);
        
        String accessToken = json.getString("access_token");
        
        System.out.println(accessToken);
        
        return accessToken;
        
    }
    
    //send mpesa request
    @Override
    public String sendRequest(String url, String requestBody){
        
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        
        HttpHeaders headers = createHeaders();
        
        HttpEntity<String> request = new HttpEntity<>(requestBody,headers);
        
        ResponseEntity<String> responseEntity = null;

        String response = null;
        try {
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            response = responseEntity.getBody();
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to send mpesa request " + e.getMessage());
            e.printStackTrace();

            JSONObject res = new JSONObject();
            res.put("response", "Failed to send request. Cant reach Safaricom");
            response = res.toString();
        }

        log.log(Level.INFO, response);
        return response;
            
    }  
    
    //create headers
    private HttpHeaders createHeaders(){
        String accessToken = null;
        try {
            accessToken = getAuthorization();
        } catch (UnsupportedEncodingException ex) {
            log.log(Level.SEVERE, null, ex);
        }
        
        log.log(Level.INFO, "Generated access token " + accessToken);
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("authorization", "Bearer " + accessToken);
        headers.add("cache-control", "no-cache");
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        return headers;
        
    }

    @Override
    public String sendMpesaResponse(String url, String mpeseResponseBody) {
        
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        
        HttpEntity<String> mpesaResponse = new HttpEntity<>(mpeseResponseBody);
        
        ResponseEntity<String> responseEntity = null;
        
        String response = null;
        try {
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, mpesaResponse, String.class);
            response = responseEntity.getBody();
        } catch (Exception e) {
            String failureMessage = "Failed to send mpesa response to owner.";
            log.log(Level.SEVERE, failureMessage + " " + e.getMessage());
            e.printStackTrace();
            
            JSONObject res = new JSONObject();
            res.put("response", failureMessage);
            response = res.toString();
        }
        
        return response;
    }


    

}
