/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.mpesaService;

import com.atlancis.paymentgateway.customEntitites.B2BRequest;
import com.atlancis.paymentgateway.customEntitites.LNMRequest;
import org.json.JSONObject;

/**
 *
 * @author nthemba
 */
public interface MpesaRequestService {
    
    String lipaNaMpesaRequest(JSONObject json);
    
    String lipaNaMpesaRequest(LNMRequest request);
    
    String registerUrlRequest();
    
    String c2bConfirmation();
    
    String c2bValidate();
    
    String c2bSimulate(String phoneNumber, Double amount);
    
    String b2bPaymentRequest(String shortcode, Double amount);
    
    String b2bPaymentRequest(B2BRequest b2BRequest);
    
    String b2cPaymentRequest(String phoneNumber, Double amount);
    
    String getaccountBalance();
    
    String transactionStatus(String phoneNumber, String transactionId);
    
}
