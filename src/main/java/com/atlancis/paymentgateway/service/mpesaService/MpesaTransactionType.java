/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.mpesaService;

import org.springframework.stereotype.Service;

/**
 *
 * @author nthemba
 */
public enum MpesaTransactionType {
    LNM_TRANS,C2B_TRANS, B2B_TRANS, B2C_TRANS
}
