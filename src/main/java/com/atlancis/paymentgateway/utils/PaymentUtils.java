/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

/**
 *
 * @author nthemba
 */
public class PaymentUtils {
    
    private static ObjectMapper mapper;

    public static String formatPhoneNumber(String phoneNumber) {
        if (isNullOrEmpty(phoneNumber)) {
            return null;
        }
        if (phoneNumber.matches("\\d{10}")) {
            return "254" + phoneNumber.substring(1);
        } else if (phoneNumber.matches("\\+\\d{12}")) {
            return phoneNumber.substring(1);
        }

        return phoneNumber;
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        //0716360959
        //254716360959
        //+254716360959
        boolean matches = phoneNumber.matches("\\d{10}|\\d{12}|\\+\\d{12}");
        return matches;
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }
    
    public static <T> T  getObject(String data, Class<T> clazz) throws IOException{
		return mapper.readValue(data, clazz);
	}

}
